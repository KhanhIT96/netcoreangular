import { Component, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-fetch-data",
  templateUrl: "./fetch-data.component.html"
})
export class FetchDataComponent {
  public forecasts: WeatherForecast[];

  constructor(http: HttpClient, @Inject("BASE_URL") baseUrl: string) {
    // console.log('result: ', baseUrl)
    http.get<WeatherForecast[]>(baseUrl + "weatherforecast").subscribe(
      result => {
        console.log('result: ', result)
        this.forecasts = result;
      },
      error => console.error("error: ", JSON.stringify(error))
    );
  }
}

interface WeatherForecast {
  date: string;
  temperatureC: number;
  temperatureF: number;
  summary: string;
}
